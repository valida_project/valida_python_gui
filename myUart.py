import numpy as np
import serial
import time



"""----------------------------------------------------------------------------
@Description:
    Convierte a 2 complemento de unisgned


-------------------------------------------------------------------------------"""
def conv_2comp(number, res):
    half_res = 1 << (res-1)
    full_res = half_res | (half_res-1)
    if (number >= half_res):
        int_sig = (full_res + 1 - number)*-1    
    else:
        int_sig = number
    return int_sig

# Template :
# 	rng1 = conv_2comp_array(rng1, 8)
def conv_2comp_array(arr, res):    
    l = [conv_2comp(arr_i, res) for arr_i in arr]
    return np.array(l)


"""-------------------------------------------------------------------------------
@Description:

@Input:

@Output:

@Arguments:

@returns:

@Template: 
    from myUart import myUart
 
    uart = myUart(com=6, br=115200)
    uart.open()

    data = np.arange(180) 
    uart.tx_array(data)

    data_rx = uart.rx_array()
 
    uart.close()
-------------------------------------------------------------------------------"""
from PyQt5 import QtCore, QtGui, QtWidgets

class Uart():
    
    def printTextBox(self, str):
        self.textbox.insertPlainText(str + '\r\n')
        self.textbox.moveCursor(QtGui.QTextCursor.End)
    
    def __init__(self, com, br, ext_ram_len, textbox):
        self.com = com
        self.br = br
        self.ext_ram_len = ext_ram_len
        self.time_1byte_sec = 1/br*12   
        self.textbox = textbox
        self.printTextBox(f'>UART object created:')
        self.printTextBox(f'   COM = {self.com}')
        self.printTextBox(f'   BR = {self.br}')
        self.printTextBox(f'   RAM_len = {self.ext_ram_len}')
        self.printTextBox(f'   time to write RAM = {self.time_1byte_sec*self.ext_ram_len} seconds')
      
      
   

    def open(self):
        self.ser = serial.Serial(f'COM{self.com}', self.br, timeout=0)
        self.ser.set_buffer_size(rx_size = 100000, tx_size = 12800)
        self.printTextBox(f'>UART COM{self.com} Opened at {self.br} bps!')
        
        self.flush()
  
    def close(self):
        self.ser.close()
        self.printTextBox(f'>UART COM{self.com} Closed!')

    def tx_array(self, arr, verbose=False):
        wr_data = bytearray(list(arr))
        # wr_data = b'abc'             
        tmp = self.ser.write(wr_data)
        if verbose:
            self.printTextBox(f'>UART sent = {tmp} bytes')
  
    def rx_array(self, verbose=False):
        n_data = self.ser.in_waiting
        
        data = self.ser.read(n_data)	
        if verbose:
            self.printTextBox(f'>UART received = {len(list(data))} bytes')
        return list(data)

    def flush(self):
        n_data = self.ser.in_waiting		
        data = self.ser.read(n_data)	
        # self.printTextBox(f'>UART buffer rx Cleared !')	

    def get_ext_ram_len(self):
        return self.ext_ram_len
    
    """---------------------------------------- 
     COMANDOS DE APPI
      ----------------------------------------"""
     
    def wait(self, sec):
        time.sleep(sec)
  
    def send_dummy(self, size):
        msg = np.zeros(size).astype(int)
        self.tx_array(msg)
        self.wait(0.1)
        self.flush()
    
    """-------------------------------------------------------------------------------
    @Description:
        Are you there?
    
    @Template: 
        uart.api_are_you_there()
    -------------------------------------------------------------------------------"""
    def api_are_you_there(self, verbose=False):

        self.tx_array([0x3F])
        self.wait(0.1)
        data_rx = self.rx_array(verbose=verbose)
        if verbose: self.printTextBox(data_rx)
        # self.printTextBox('are you there?...')
        self.printTextBox(f'>I am here!\nFSM_version : {data_rx[1]}')
 
    """-------------------------------------------------------------------------------

    Test de comando de guardar en buffer config

    -------------------------------------------------------------------------------
        
      @Template:
           data_tx = np.arange(64)

        uart.api_LUTRAMbuffer_write(data_tx)
        data_rx = uart.api_LUTRAMbuffer_read(verbose=False)
        same_data_flag = np.all(data_tx==data_rx)
        self.printTextBox(f'Test write buffer appi : ')
        self.printTextBox(f'Writing {len(data_tx)} bytes...Reading buffer...\nsame data? = {same_data_flag}')
        if same_data_flag:  self.printTextBox('Test passed succesfully')
        else : self.printTextBox('Test passed ERROR')
    -----------------------------------------------------------------------------"""
    # Guarda en Config Buffer
    def api_LUTRAMbuffer_write(self, payload, verbose=False):
        data_tx = np.append(len(payload), payload)
        comand = 0x43

        msg = np.append(comand, data_tx)
        self.tx_array(msg, verbose=verbose)
        self.wait(0.1)
        self.flush()
        self.printTextBox(">Config Buffer Write Ok")
  
    # Lee de config Buffer
    def api_LUTRAMbuffer_read(self, verbose=False):
        comand = 0x63		
        msg = [comand]

        self.tx_array(msg)
        self.wait(0.1)
        data_rx = self.rx_array(verbose=verbose)
        if verbose: self.printTextBox(','.join([str(data_i) for data_i in data_rx]))       

        return data_rx[1:]

    """
    @Template:
        data_tx = np.random.randint(0,255,size=(512))

        uart.api_ExtRAM_write(data_tx, verbose=True)
        data_rx = uart.api_ExtRAM_read(verbose=True)
    
        same_data_flag = np.all(data_tx==data_rx)
        self.printTextBox(f'Test RAM : ')
        self.printTextBox(f'Writing {len(data_tx)} bytes...Reading ...\nsame data? = {same_data_flag}')
        if same_data_flag:  self.printTextBox('Test passed succesfully')
        else : self.printTextBox('Test passed ERROR')
    """
    def api_ExtRAM_write(self, payload, verbose=False):
        
        assert	len(payload) == self.ext_ram_len, f"Error payload len, the RAM write is done in complete blocks (see VHDL code to know the block size of the RAM), the block for this RAM is {self.ext_ram_len} bytes but {len(payload)} is given"

        msg = np.append(0x57, payload)
        self.tx_array(msg, verbose=verbose)
        self.wait(self.ext_ram_len * self.time_1byte_sec)
        self.flush()
  
    
    def api_ExtRAM_read(self, verbose=False):
        msg = [0x77]

        self.tx_array(msg, verbose=verbose)
        self.wait(self.ext_ram_len * self.time_1byte_sec)
        data_rx = self.rx_array(verbose=verbose)
        if verbose: self.printTextBox(data_rx)
        return data_rx[1:]

    """-------------------------------------------------------------------------------
        TODO: Rellenar
     @Description:

    @Input:
    
    @Output:
    
    @Arguments:
    
    @returns:
    
    @Template: 
    
    -------------------------------------------------------------------------------"""
    def api_audio_get(self, n_samples, sample_rate, verbose=False):
        assert	n_samples < 65536, f"Error, max number of samples 65536, actual value = {n_samples}"

        msg = [0x53] # command
        msg.append(n_samples>>8)
        msg.append(n_samples & 0xFF)
  
        self.printTextBox(f'>sampling audio...')
        self.printTextBox(f'\t{n_samples=}')
        self.printTextBox(f'\t{sample_rate=}')
  
        self.tx_array(msg, verbose=verbose)
        self.wait(1/sample_rate*n_samples + 0.1)
        data_rx = self.rx_array(verbose=verbose)
        # self.printTextBox(f'data_rx len = {len(data_rx)}')
        if verbose: self.printTextBox(data_rx)
  
        # trata los datos, recibimos 2 bytes por sample
        uint16 = uint8_to_uint16(data_rx[3:])
  
        # convierte a signo
        bits = 16
        audio_signed = conv_2comp_array(uint16, bits)

        self.printTextBox(f'>ok')
        self.flush()
        return audio_signed
        # return uint16

    def api_audio_cropped_get(self, n_samples, sample_rate, verbose=False):
        assert	n_samples < 65536, f"Error, max number of samples 65536, actual value = {n_samples}"

        msg = [0x56] # command
        msg.append(n_samples>>8)
        msg.append(n_samples & 0xFF)
  
        self.printTextBox(f'>sampling audio Cropped...')
        self.printTextBox(f'\t{n_samples=}')
        self.printTextBox(f'\t{sample_rate=}')
  
        self.tx_array(msg, verbose=verbose)
        self.wait(1/sample_rate*n_samples + 0.1)
        data_rx = self.rx_array(verbose=verbose)
        # self.printTextBox(f'data_rx len = {len(data_rx)}')
        if verbose: self.printTextBox(data_rx)
  
        # trata los datos, recibimos 2 bytes por sample
        uint16 = uint8_to_uint16(data_rx[3:])
  
        # convierte a signo
        bits = 16
        audio_signed = conv_2comp_array(uint16, bits)

        self.printTextBox(f'>ok')
        self.flush()
        return audio_signed
        # return uint16

    """-------------------------------------------------------------------------------

     @Description:

    @Input:
    
    @Output:
    
    @Arguments:
    
    @returns:
    
    @Template: 
    
    -------------------------------------------------------------------------------"""
    def api_afe_get(self, n_samples, sample_rate, verbose=False):
        assert	n_samples < 65536, f"Error, max number of samples 65536, actual value = {n_samples}"

        msg = [0x54] # command
        msg.append(n_samples>>8)
        msg.append(n_samples & 0xFF)
  
        self.printTextBox(f'>sampling AFE...')
        self.printTextBox(f'\t{n_samples=}')
        self.printTextBox(f'\t{sample_rate=}')
  
        self.tx_array(msg, verbose=verbose)

        CONSTANT_L_FROM_RTL_MODULE = 128
        CONSTANT_N_FROM_RTL_MODULE = 32
        self.wait(1/sample_rate*CONSTANT_L_FROM_RTL_MODULE + 3)
        data_rx = self.rx_array(verbose=verbose)
        # self.printTextBox(f'data_rx len = {len(data_rx)}')
        if verbose: self.printTextBox(data_rx)
        
        # toma solo desde el 3, ya que los primeros 3 bytes 
        # son de echo
        data_rx = data_rx[3:]
        # convierte a signo
        bits = 8
        afe_signed = conv_2comp_array(data_rx, bits)
        # The data has to be scaled due to only 8 bits can be sent by UART
        # but the original output is 9 bits
        SCALE = 2
        afe_signed *= SCALE
        # order
        afe_signed = afe_signed.reshape(int(len(afe_signed)/CONSTANT_N_FROM_RTL_MODULE), CONSTANT_N_FROM_RTL_MODULE)

        self.printTextBox(f'>ok')
        self.flush()
        return afe_signed
        # return uint16
    """-------------------------------------------------------------------------------

     @Description:

    @Input:
    
    @Output:
    
    @Arguments:
    
    @returns:
    
    @Template: 
    
    -------------------------------------------------------------------------------"""
    def api_afe_test(self, csv, rtl_n, rtl_l, verbose=False):

        n_samples = len(csv)
        n_samples_to_wait = int(n_samples/rtl_l)

        msg = [0x55] # command
        msg.append(n_samples_to_wait>>8)
        msg.append(n_samples_to_wait & 0xFF)
        self.tx_array(msg, verbose=verbose)
        self.wait(0.1)
        self.flush()

  
        self.printTextBox(f'>sending csv audio...')
        self.printTextBox(f'\t{n_samples=}')
        self.printTextBox(f'>waiting for {n_samples_to_wait} afe samples...')

        # The data has to be scaled due to only 8 bits can be sent
        # but the original output is 9 bits
        SCALE = 2 
        afe_out = []
        csv = [csv_i&0xFF for csv_i in csv]
        for i in range(n_samples_to_wait):
            msg = csv[i*rtl_l:(i+1)*rtl_l]
            self.tx_array(msg, verbose=False)
            # self.printTextBox(f'Tx {len(msg) =}')
            self.wait(0.01)
            data_rx = self.rx_array(verbose=False)
            # self.printTextBox(f'{len(data_rx) = }')
            # convierte a signo
            bits = 8
            afe_signed = conv_2comp_array(data_rx, bits)
            afe_signed *= SCALE
            afe_out.append(afe_signed[:32])     # por si llegan mas de 32... 

        self.printTextBox(f'>ok')
        self.flush()
        return np.array(afe_out)
        # return uint16


def uint8_to_uint16(from_buffer):
    if (len(from_buffer)%2 != 0 ): raise ValueError('el buffer no tiene longitud par')
    
    n_samples = len(from_buffer)
    uint16 = []
    for i in range(0, n_samples, 2):
            tmp = from_buffer[i]<<8
            tmp += from_buffer[i+1]
            uint16.append(tmp)
    return uint16

def makeUint16(msb, lsb):
    return ((msb<<8) & 0xFF00) + lsb

def makeSint16(msb, lsb):
    tmp =  makeUint16(msb, lsb)
    return conv_2comp(tmp, res=16)