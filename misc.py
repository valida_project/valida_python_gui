import numpy as np


def norm_audio(audio):  
  mean = np.mean(audio)
  audio = np.array(audio) - mean
  max = np.max(np.abs(audio))
  audio_n = np.int16(audio/max * (2**15-1))
  return audio_n

def playwWavFromArray(array, sample_rate):
    import sounddevice as sd
    sd.default.samplerate = sample_rate
    sd.play(array, blocking=True)