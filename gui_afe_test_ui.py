# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui_afe_test.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(735, 824)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.plotAudio = QtWidgets.QWidget(self.centralwidget)
        self.plotAudio.setGeometry(QtCore.QRect(10, 0, 591, 121))
        self.plotAudio.setObjectName("plotAudio")
        self.plotAudioLayout = QtWidgets.QVBoxLayout(self.plotAudio)
        self.plotAudioLayout.setContentsMargins(0, 0, 0, 0)
        self.plotAudioLayout.setObjectName("plotAudioLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 270, 281, 51))
        self.groupBox.setObjectName("groupBox")
        self.comboBox_PORTS = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_PORTS.setGeometry(QtCore.QRect(90, 20, 69, 22))
        self.comboBox_PORTS.setObjectName("comboBox_PORTS")
        self.OpenPort_button = QtWidgets.QPushButton(self.groupBox)
        self.OpenPort_button.setGeometry(QtCore.QRect(10, 20, 75, 23))
        self.OpenPort_button.setObjectName("OpenPort_button")
        self.CleanBuffer_button = QtWidgets.QPushButton(self.groupBox)
        self.CleanBuffer_button.setGeometry(QtCore.QRect(170, 20, 75, 23))
        self.CleanBuffer_button.setObjectName("CleanBuffer_button")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 380, 281, 181))
        self.groupBox_2.setObjectName("groupBox_2")
        self.comboBox_Gain = QtWidgets.QComboBox(self.groupBox_2)
        self.comboBox_Gain.setGeometry(QtCore.QRect(10, 40, 71, 22))
        self.comboBox_Gain.setEditable(False)
        self.comboBox_Gain.setObjectName("comboBox_Gain")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setGeometry(QtCore.QRect(10, 20, 91, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_2.setGeometry(QtCore.QRect(100, 20, 91, 16))
        self.label_2.setObjectName("label_2")
        self.textbox_audio_thresh = QtWidgets.QPlainTextEdit(self.groupBox_2)
        self.textbox_audio_thresh.setGeometry(QtCore.QRect(110, 40, 41, 21))
        self.textbox_audio_thresh.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_audio_thresh.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_audio_thresh.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_audio_thresh.setDocumentTitle("")
        self.textbox_audio_thresh.setObjectName("textbox_audio_thresh")
        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setGeometry(QtCore.QRect(190, 20, 91, 16))
        self.label_3.setObjectName("label_3")
        self.comboBox_Audio_Resolution = QtWidgets.QComboBox(self.groupBox_2)
        self.comboBox_Audio_Resolution.setGeometry(QtCore.QRect(190, 40, 71, 22))
        self.comboBox_Audio_Resolution.setEditable(False)
        self.comboBox_Audio_Resolution.setObjectName("comboBox_Audio_Resolution")
        self.pushButton_config_send = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_config_send.setGeometry(QtCore.QRect(10, 150, 75, 23))
        self.pushButton_config_send.setObjectName("pushButton_config_send")
        self.pushButton_config_read = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_config_read.setGeometry(QtCore.QRect(90, 150, 75, 23))
        self.pushButton_config_read.setObjectName("pushButton_config_read")
        self.textbox_delta = QtWidgets.QPlainTextEdit(self.groupBox_2)
        self.textbox_delta.setGeometry(QtCore.QRect(130, 70, 41, 21))
        self.textbox_delta.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_delta.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_delta.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_delta.setDocumentTitle("")
        self.textbox_delta.setObjectName("textbox_delta")
        self.label_8 = QtWidgets.QLabel(self.groupBox_2)
        self.label_8.setGeometry(QtCore.QRect(10, 70, 91, 16))
        self.label_8.setObjectName("label_8")
        self.textbox_valida_thresh_mac_high = QtWidgets.QPlainTextEdit(self.groupBox_2)
        self.textbox_valida_thresh_mac_high.setGeometry(QtCore.QRect(130, 90, 41, 21))
        self.textbox_valida_thresh_mac_high.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_valida_thresh_mac_high.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_valida_thresh_mac_high.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_valida_thresh_mac_high.setDocumentTitle("")
        self.textbox_valida_thresh_mac_high.setObjectName("textbox_valida_thresh_mac_high")
        self.label_9 = QtWidgets.QLabel(self.groupBox_2)
        self.label_9.setGeometry(QtCore.QRect(10, 90, 101, 16))
        self.label_9.setObjectName("label_9")
        self.textbox_valida_thresh_mac_low = QtWidgets.QPlainTextEdit(self.groupBox_2)
        self.textbox_valida_thresh_mac_low.setGeometry(QtCore.QRect(130, 110, 41, 21))
        self.textbox_valida_thresh_mac_low.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_valida_thresh_mac_low.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_valida_thresh_mac_low.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_valida_thresh_mac_low.setDocumentTitle("")
        self.textbox_valida_thresh_mac_low.setObjectName("textbox_valida_thresh_mac_low")
        self.label_10 = QtWidgets.QLabel(self.groupBox_2)
        self.label_10.setGeometry(QtCore.QRect(10, 110, 91, 16))
        self.label_10.setObjectName("label_10")
        self.console_textbox = QtWidgets.QTextEdit(self.centralwidget)
        self.console_textbox.setGeometry(QtCore.QRect(340, 280, 341, 331))
        self.console_textbox.setObjectName("console_textbox")
        self.button_r_u_there = QtWidgets.QPushButton(self.centralwidget)
        self.button_r_u_there.setGeometry(QtCore.QRect(340, 620, 81, 23))
        self.button_r_u_there.setObjectName("button_r_u_there")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 580, 281, 71))
        self.groupBox_3.setObjectName("groupBox_3")
        self.label_4 = QtWidgets.QLabel(self.groupBox_3)
        self.label_4.setGeometry(QtCore.QRect(10, 20, 51, 16))
        self.label_4.setObjectName("label_4")
        self.textbox_audio_n_samples = QtWidgets.QPlainTextEdit(self.groupBox_3)
        self.textbox_audio_n_samples.setGeometry(QtCore.QRect(10, 40, 51, 21))
        self.textbox_audio_n_samples.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_audio_n_samples.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_audio_n_samples.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_audio_n_samples.setDocumentTitle("")
        self.textbox_audio_n_samples.setObjectName("textbox_audio_n_samples")
        self.label_5 = QtWidgets.QLabel(self.groupBox_3)
        self.label_5.setGeometry(QtCore.QRect(80, 20, 21, 16))
        self.label_5.setObjectName("label_5")
        self.textbox_audio_sr = QtWidgets.QPlainTextEdit(self.groupBox_3)
        self.textbox_audio_sr.setEnabled(False)
        self.textbox_audio_sr.setGeometry(QtCore.QRect(70, 40, 41, 21))
        self.textbox_audio_sr.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_audio_sr.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_audio_sr.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_audio_sr.setDocumentTitle("")
        self.textbox_audio_sr.setObjectName("textbox_audio_sr")
        self.pushButton_sample_audio_original = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton_sample_audio_original.setGeometry(QtCore.QRect(190, 10, 75, 23))
        self.pushButton_sample_audio_original.setObjectName("pushButton_sample_audio_original")
        self.pushButton_sample_audio_cropped = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton_sample_audio_cropped.setGeometry(QtCore.QRect(190, 40, 75, 23))
        self.pushButton_sample_audio_cropped.setObjectName("pushButton_sample_audio_cropped")
        self.groupBox_4 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 660, 281, 111))
        self.groupBox_4.setObjectName("groupBox_4")
        self.label_6 = QtWidgets.QLabel(self.groupBox_4)
        self.label_6.setGeometry(QtCore.QRect(10, 20, 51, 16))
        self.label_6.setObjectName("label_6")
        self.textbox_afe_n_samples = QtWidgets.QPlainTextEdit(self.groupBox_4)
        self.textbox_afe_n_samples.setGeometry(QtCore.QRect(10, 40, 51, 21))
        self.textbox_afe_n_samples.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_afe_n_samples.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_afe_n_samples.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_afe_n_samples.setDocumentTitle("")
        self.textbox_afe_n_samples.setObjectName("textbox_afe_n_samples")
        self.label_7 = QtWidgets.QLabel(self.groupBox_4)
        self.label_7.setGeometry(QtCore.QRect(80, 20, 21, 16))
        self.label_7.setObjectName("label_7")
        self.textbox_afe_sr = QtWidgets.QPlainTextEdit(self.groupBox_4)
        self.textbox_afe_sr.setEnabled(False)
        self.textbox_afe_sr.setGeometry(QtCore.QRect(70, 40, 41, 21))
        self.textbox_afe_sr.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textbox_afe_sr.setInputMethodHints(QtCore.Qt.ImhNone)
        self.textbox_afe_sr.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textbox_afe_sr.setDocumentTitle("")
        self.textbox_afe_sr.setObjectName("textbox_afe_sr")
        self.pushButton_sample_afe = QtWidgets.QPushButton(self.groupBox_4)
        self.pushButton_sample_afe.setGeometry(QtCore.QRect(190, 30, 75, 23))
        self.pushButton_sample_afe.setObjectName("pushButton_sample_afe")
        self.pushButton_afe_send_csv = QtWidgets.QPushButton(self.groupBox_4)
        self.pushButton_afe_send_csv.setGeometry(QtCore.QRect(190, 80, 75, 23))
        self.pushButton_afe_send_csv.setObjectName("pushButton_afe_send_csv")
        self.frame = QtWidgets.QFrame(self.groupBox_4)
        self.frame.setGeometry(QtCore.QRect(0, 60, 281, 16))
        self.frame.setFrameShape(QtWidgets.QFrame.HLine)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.plotAFE = QtWidgets.QWidget(self.centralwidget)
        self.plotAFE.setGeometry(QtCore.QRect(10, 130, 591, 131))
        self.plotAFE.setObjectName("plotAFE")
        self.plotAFELayout = QtWidgets.QVBoxLayout(self.plotAFE)
        self.plotAFELayout.setContentsMargins(0, 0, 0, 0)
        self.plotAFELayout.setObjectName("plotAFELayout")
        self.button_audio_save_csv = QtWidgets.QPushButton(self.centralwidget)
        self.button_audio_save_csv.setGeometry(QtCore.QRect(610, 20, 81, 23))
        self.button_audio_save_csv.setObjectName("button_audio_save_csv")
        self.button_Audio_play = QtWidgets.QPushButton(self.centralwidget)
        self.button_Audio_play.setGeometry(QtCore.QRect(610, 50, 81, 23))
        self.button_Audio_play.setObjectName("button_Audio_play")
        self.button_AFE_save_csv = QtWidgets.QPushButton(self.centralwidget)
        self.button_AFE_save_csv.setGeometry(QtCore.QRect(610, 130, 81, 23))
        self.button_AFE_save_csv.setObjectName("button_AFE_save_csv")
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(0, 111, 701, 20))
        self.frame_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 735, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "AFE test v0.0"))
        self.groupBox.setTitle(_translate("MainWindow", "UART"))
        self.OpenPort_button.setText(_translate("MainWindow", "OPEN"))
        self.CleanBuffer_button.setText(_translate("MainWindow", "Clean Buffer"))
        self.groupBox_2.setTitle(_translate("MainWindow", "CONFIG"))
        self.label.setText(_translate("MainWindow", "Gain microphone"))
        self.label_2.setText(_translate("MainWindow", "Threshold Audio"))
        self.textbox_audio_thresh.setPlainText(_translate("MainWindow", "100"))
        self.label_3.setText(_translate("MainWindow", "Audio resolution"))
        self.pushButton_config_send.setText(_translate("MainWindow", "Write"))
        self.pushButton_config_read.setText(_translate("MainWindow", "Read"))
        self.textbox_delta.setPlainText(_translate("MainWindow", "2"))
        self.label_8.setText(_translate("MainWindow", "delta"))
        self.textbox_valida_thresh_mac_high.setPlainText(_translate("MainWindow", "8000"))
        self.label_9.setText(_translate("MainWindow", "Threshold MAC high"))
        self.textbox_valida_thresh_mac_low.setPlainText(_translate("MainWindow", "12000"))
        self.label_10.setText(_translate("MainWindow", "Threshold MAC low"))
        self.button_r_u_there.setText(_translate("MainWindow", "Are you there?"))
        self.groupBox_3.setTitle(_translate("MainWindow", "AUDIO"))
        self.label_4.setText(_translate("MainWindow", "N samples"))
        self.textbox_audio_n_samples.setPlainText(_translate("MainWindow", "8000"))
        self.label_5.setText(_translate("MainWindow", "SR"))
        self.textbox_audio_sr.setPlainText(_translate("MainWindow", "8000"))
        self.pushButton_sample_audio_original.setText(_translate("MainWindow", "Get Original"))
        self.pushButton_sample_audio_cropped.setText(_translate("MainWindow", "Get Cropped"))
        self.groupBox_4.setTitle(_translate("MainWindow", "AFE"))
        self.label_6.setText(_translate("MainWindow", "N samples"))
        self.textbox_afe_n_samples.setPlainText(_translate("MainWindow", "8000"))
        self.label_7.setText(_translate("MainWindow", "SR"))
        self.textbox_afe_sr.setPlainText(_translate("MainWindow", "8000"))
        self.pushButton_sample_afe.setText(_translate("MainWindow", "from MIC"))
        self.pushButton_afe_send_csv.setText(_translate("MainWindow", "Test CSV"))
        self.button_audio_save_csv.setText(_translate("MainWindow", "Save CSV"))
        self.button_Audio_play.setText(_translate("MainWindow", "Play"))
        self.button_AFE_save_csv.setText(_translate("MainWindow", "Save CSV"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

